
const path = require('path');

module.exports = {
  devtool: 'eval-source-map',

  entry: './src/index.js',
 
  mode: 'development',
 
  module: {
    rules: [{
      // escludi la cartella node_modules dalla ricerca dei file in js
      exclude: /node_modules/,
      //metteremo quindi prima Babel di Prettier così prima "indenta" e poi "transpila"
      use: [{
        loader:'babel-loader',
        options: {
          presets: [
            ['@babel/preset-env', { targets: "defaults" }]
          ]
        }
      }],
      test: /\.jsx?$/,
    }]
  },
  
  output: {
   
    path: path.resolve(__dirname, 'dist'),
  
    filename: 'bundle.js',
  },
};